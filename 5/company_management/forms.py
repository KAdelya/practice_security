from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length


class DepartmentForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=1, max=64)])


class EmployeeForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=1, max=64)])
    surname = StringField('Surname', validators=[DataRequired(), Length(min=1, max=64)])
    department_name = StringField('department_name', validators=[DataRequired()])
