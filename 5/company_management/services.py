import sqlite3

from flask_sqlalchemy import Pagination
from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from company_management.forms import DepartmentForm, EmployeeForm
from company_management.models import Department, Employee
from database import db


class DatabaseError(Exception):
    pass


def get_paginated_departments_list(page: int) -> Pagination:
    return Department.query.paginate(page, 5)


def create_department(form: DepartmentForm):
    try:
        db.session.add(Department(name=form.name.data))
        db.session.commit()
    except SQLAlchemyError as e:
        db.session.rollback()
        text = 'Unknown error'
        if isinstance(e, IntegrityError):
            text = 'Name of department should be unique'
        raise DatabaseError(text)


def create_employee(form: EmployeeForm):
    try:
        department = Department.query.filter_by(name=form.department_name.data).first()
        if department is None:
            raise DatabaseError(f'Department with name {form.department_name.data} does not exist')
        db.session.add(Employee(name=form.name.data, surname=form.surname.data, department=department))
        db.session.commit()
    except SQLAlchemyError as e:
        db.session.rollback()
        raise DatabaseError(f'Unknown error')


def get_department_by_name_part(name: str):
    con = sqlite3.connect('db.sqlite3')
    cur1 = con.cursor()
    cur1.execute("SELECT * FROM departments WHERE name = '{0}'".format(name))
    data = cur1.fetchall()
    con.commit()
    con.close()
    # return Department.query.filter(Department.name.ilike(f'%{name}%')).limit(5).all()
    return data
