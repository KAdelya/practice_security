import sqlite3

from flask import request, redirect, url_for, flash, render_template, Blueprint, abort
from flask_login import login_user, login_required, logout_user, current_user
from database import db
from accounts.services import find_by_username_and_password, IncorrectPasswordError, NotFoundError, find_user_by_id

accounts_blueprint = Blueprint('accounts', __name__, template_folder='templates')


@accounts_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        remember_me = len(request.form.getlist('remember_me')) > 0
        try:
            user = find_by_username_and_password(username, password)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('accounts.login'))
        else:
            login_user(user, remember=remember_me)
            return redirect(url_for('company_management.main_page'))
    return render_template('accounts/login.html')


@accounts_blueprint.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('company_management.main_page'))


@accounts_blueprint.route("/user/<int:user_id>", methods=["GET"])
@login_required
def user_info(user_id: int):
    try:
        user = find_user_by_id(user_id)
        return render_template('accounts/user_info.html', user=user)
    except NotFoundError:
        abort(404)


@accounts_blueprint.route("/user", methods=["GET"])
@login_required
def current_user_info():
    return render_template('accounts/user_info.html', user=current_user)


@accounts_blueprint.route("/change_data", methods=["GET", "POST"])
@login_required
def change_data():
    if request.method == 'POST':
        current_user.username = request.form['username']
        db.session.commit()
        return redirect(url_for('company_management.main_page'))
    return render_template('accounts/change_data.html')


@accounts_blueprint.route("/all_users", methods=["GET", "POST"])
@login_required
def all_users():
    if request.method == 'GET':
        username_form = request.args.get('username_form', '')
        con = sqlite3.connect('C:\\Users\\Home\\Desktop\\5\\db.sqlite3')
        cur1 = con.cursor()
        cur1.execute("SELECT * FROM users WHERE username = '{0}'".format(username_form))
        data = cur1.fetchall()
        con.commit()
        con.close()
        return render_template('accounts/search_users.html', data=data)
    return render_template('accounts/all_users.html')
