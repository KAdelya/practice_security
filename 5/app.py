import os

from flask import Flask

from accounts.controllers import accounts_blueprint
from accounts.login_manager import login_manager
# noinspection PyUnresolvedReferences
from accounts.models import User
from company_management.controllers import company_management_blueprint
# noinspection PyUnresolvedReferences
from company_management.models import Department, Employee
from database import db, migrate
# from error_handlers import page_not_found
from error_handlers import page_not_found
from local_configs import Configuration

app = Flask(__name__)

if not os.getenv('IS_PRODUCTION', None):
    app.config.from_object(Configuration)

db.init_app(app)
migrate.init_app(app, db)
login_manager.init_app(app)

app.register_blueprint(accounts_blueprint, url_prefix='/accounts')
app.register_blueprint(company_management_blueprint, url_prefix='/')
app.register_error_handler(404, page_not_found)


if __name__ == '__main__':
    app.run(debug=True)
